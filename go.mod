module meepo

go 1.12

replace (
	ake.alauda.io/cluster-registry => bitbucket.org/mathildetech/cluster-registry v0.0.0-20191022110458-414bf9199ac6
	alauda.io/warpgate v0.0.1 => bitbucket.org/mathildetech/warpgate v0.0.1
	k8s.io/api => k8s.io/api v0.0.0-20190313235455-40a48860b5ab
	k8s.io/apiextensions-apiserver => k8s.io/apiextensions-apiserver v0.0.0-20190606210616-f848dc7be4a4
	k8s.io/apimachinery => k8s.io/apimachinery v0.0.0-20190313205120-d7deff9243b1
	k8s.io/client-go => k8s.io/client-go v11.0.0+incompatible
	k8s.io/kubectl/pkg/framework/openapi => ./vendor/kubectl/pkg/framework/openapi
)

require (
	alauda.io/warpgate v0.0.1
	bitbucket.org/mathildetech/auth-controller2 v0.0.0-20191104032454-aa8b8a70136c
	github.com/alauda/helm-crds v0.0.0-20190915014518-6c1be05f7d6e
	github.com/go-logr/logr v0.1.0
	github.com/go-openapi/spec v0.19.4 // indirect
	github.com/imdario/mergo v0.3.7 // indirect
	github.com/onsi/ginkgo v1.10.1
	github.com/onsi/gomega v1.7.0
	github.com/pborman/uuid v1.2.0 // indirect
	github.com/pkg/errors v0.8.1
	github.com/spf13/cobra v0.0.5 // indirect
	github.com/stretchr/testify v1.4.0 // indirect
	gopkg.in/fatih/set.v0 v0.2.1
	k8s.io/api v0.0.0-20191109101513-0171b7c15da1
	k8s.io/apiextensions-apiserver v0.0.0
	k8s.io/apimachinery v0.0.0-20191109100838-fee41ff082ed
	k8s.io/client-go v11.0.1-0.20190409021438-1a26190bd76a+incompatible
	k8s.io/cluster-registry v0.0.6
	k8s.io/component-base v0.0.0-20191111061729-cca8f4f7ce4d // indirect
	k8s.io/kubectl/pkg/framework/openapi v0.1.0 // indirect
	sigs.k8s.io/controller-runtime v0.2.2
	sigs.k8s.io/kubefed v0.1.0-rc6.0.20191106080951-bd265062af24
)
