package util

import (
	"context"
	"fmt"
	"gopkg.in/fatih/set.v0"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	infrastructurev1 "meepo/api/v1alpha1"
	"os"
	kubefedv1beta1 "sigs.k8s.io/kubefed/pkg/apis/core/v1beta1"
	"time"
)

const (
	TokenKey = "token"

	ErrNoAvailableServerEndpoint = BuildClientErr("could not found an avaliable server endpoint from cluster config.")
	ErrAuthSecretNotProvided     = BuildClientErr("Secret Reference is not provided fully in the cluster config.")
	ErrAuthSecretFetchFailed     = BuildClientErr("Failed to fetch the secret resource specified in cluster config.")
	ErrAuthSecretInvalid         = BuildClientErr("Auth secret data is invalid, no token field found.")
	FedPlaneNamespace            = "kube-federation-system"
	ClusterFedFinalizers         = "clusterfed"
	TypeFedMember                = "FedMember"
)

var (
	FederationName     = "federation.%s/name"
	FederationHostName = "federation.%s/hostname"
)

type BuildClientErr string

func (e BuildClientErr) Error() string {
	return string(e)
}

// GetEnv get value from os environment, if not available, then return fallback.
func GetEnv(key, fallback string) string {
	value, exists := os.LookupEnv(key)
	if exists && len(value) != 0 {
		return value
	}
	return fallback
}

func GetClusterSetFromKubeFed(clusterList *kubefedv1beta1.KubeFedClusterList) set.Interface {
	clusters := set.New(set.ThreadSafe)
	for _, kubeFedCluster := range clusterList.Items {
		clusters.Add(kubeFedCluster.Name)
	}
	return clusters
}

func GetClusterSetFromAlaudaFederation(clusterList []*infrastructurev1.FederatedCluster) set.Interface {
	clusters := set.New(set.ThreadSafe)
	for _, kubeFedCluster := range clusterList {
		clusters.Add(kubeFedCluster.Name)
	}
	return clusters
}

func PopulateLabels(labelBaseDomain string) {
	FederationName = fmt.Sprintf(FederationName, labelBaseDomain)
	FederationHostName = fmt.Sprintf(FederationHostName, labelBaseDomain)
}

func NewHostNamespace() *corev1.Namespace {
	labels := map[string]string{
		"name": FedPlaneNamespace,
	}

	return &corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name:   FedPlaneNamespace,
			Labels: labels,
		},
	}
}

func RemoveSliceItem(items []string, target string) (result []string) {
	for _, item := range items {
		if item != target {
			result = append(result, item)
		}
	}

	return result
}

func LoopUntil(ctx context.Context, interval time.Duration, maxRetries int, f func() (bool, error)) error {
	count := 0
	for {
		if stop, err := f(); err != nil {
			if count++; count > maxRetries {
				return err
			}
		} else if stop {
			break
		}
		select {
		case <-time.After(interval):
		case <-ctx.Done():
			return fmt.Errorf("execute canceled")
		}
	}
	return nil
}
