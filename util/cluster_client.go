package util

import (
	"context"
	"github.com/go-logr/logr"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	clusterregistryv1alpha1 "k8s.io/cluster-registry/pkg/apis/clusterregistry/v1alpha1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

func BuildClientClientsetFromClusterRegistry(ctx context.Context, logger logr.Logger, client client.Client, c *clusterregistryv1alpha1.Cluster) (*kubernetes.Clientset, error) {
	config, err := BuildClientConfigFromClusterRegistry(ctx, logger, client, c)
	if err != nil {
		return &kubernetes.Clientset{}, err
	}
	clientSet, err := kubernetes.NewForConfig(config)
	return clientSet, err
}

func BuildClientConfigFromClusterRegistry(ctx context.Context, logger logr.Logger, client client.Client, c *clusterregistryv1alpha1.Cluster) (*rest.Config, error) {
	restConfig, err := buildBasicRestConfig(c)
	if err != nil {
		return restConfig, err
	}

	if c.Spec.AuthInfo.Controller == nil || c.Spec.AuthInfo.Controller.Name == "" ||
		c.Spec.AuthInfo.Controller.Namespace == "" {
		return restConfig, ErrAuthSecretNotProvided
	}

	secret := &corev1.Secret{}
	err = client.Get(ctx,
		types.NamespacedName{
			Namespace: c.Spec.AuthInfo.Controller.Namespace,
			Name:      c.Spec.AuthInfo.Controller.Name,
		},
		secret)
	if err != nil {
		logger.Error(err, "unable to fetch secret specified in cluster")
		return restConfig, ErrAuthSecretFetchFailed
	}

	token, exist := secret.Data[TokenKey]
	if !exist {
		return restConfig, ErrAuthSecretInvalid
	}

	restConfig.BearerToken = string(token[:])
	return restConfig, err
}

func buildBasicRestConfig(c *clusterregistryv1alpha1.Cluster) (*rest.Config, error) {
	if len(c.Spec.KubernetesAPIEndpoints.ServerEndpoints) == 0 || c.Spec.KubernetesAPIEndpoints.ServerEndpoints[0].ServerAddress == "" {
		return &rest.Config{}, ErrNoAvailableServerEndpoint
	}
	//Endpoint := fmt.Sprintf("%s", "https://127.0.0.1:9000")
	config := &rest.Config{
		Host: c.Spec.KubernetesAPIEndpoints.ServerEndpoints[0].ServerAddress,
		//Host: erebusEndpoint + "/kubernetes/" + c.ObjectMeta.Name,
	}
	if len(c.Spec.KubernetesAPIEndpoints.CABundle) == 0 {
		config.TLSClientConfig.Insecure = true
	} else {
		config.TLSClientConfig.CAData = c.Spec.KubernetesAPIEndpoints.CABundle
	}

	return config, nil
}
