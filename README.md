## meepo
meepo 是alauda 联邦集群的控制器，主要负责联邦集群的创建，集群成员的管理（add，remove），并且根据当前项目的绑定，自动修改项目下的集群列表。
meepo带有mutating 和 validate webhook，校验一个联邦只能有一个主，集群只能在一个联邦内（后续pr陆续实现）。meepo 通过kubebuilder2 脚手架编写；
webhook 的cert 通过cert-manager 解决。
meepo watch的CRD 为Clusterfeds.
