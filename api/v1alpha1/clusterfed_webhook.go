/*

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	wg "alauda.io/warpgate"
	"context"
	"fmt"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/util/validation/field"
	"net/http"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	logf "sigs.k8s.io/controller-runtime/pkg/runtime/log"
	"sigs.k8s.io/controller-runtime/pkg/webhook"
)

// log is for logging in this package.
var clusterfedlog = logf.Log.WithName("clusterfed-resource")
var controlMagager ctrl.Manager

func (r *Clusterfed) SetupWebhookWithManager(mgr ctrl.Manager) error {
	controlMagager = mgr
	return ctrl.NewWebhookManagedBy(mgr).
		For(r).
		Complete()
}

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!

// +kubebuilder:webhook:path=/mutate-infrastructure-alauda-io-v1alpha1-clusterfed,mutating=true,failurePolicy=fail,groups=infrastructure.alauda.io,resources=clusterfeds,verbs=create,versions=v1alpha1,name=mclusterfed.kb.io

var _ webhook.Defaulter = &Clusterfed{}

// Default implements webhook.Defaulter so a webhook will be registered for the type
func (object *Clusterfed) Default() {
	clusterfedlog.Info("default", "name", object.Name)

	if object.Spec.UnjoinPolicy == "" {
		object.Spec.UnjoinPolicy = Delete
	}
}

// TODO(user): change verbs to "verbs=create;update;delete" if you want to enable deletion validation.
// +kubebuilder:webhook:verbs=create;update,path=/validate-infrastructure-alauda-io-v1alpha1-clusterfed,mutating=false,failurePolicy=fail,groups=infrastructure.alauda.io,resources=clusterfeds,versions=v1alpha1,name=vclusterfed.kb.io

var _ webhook.Validator = &Clusterfed{}

// ValidateCreate implements webhook.Validator so a webhook will be registered for the type
func (r *Clusterfed) ValidateCreate() error {
	clusterfedlog.Info("validate create", "name", r.Name)
	return r.validateClusterfed()
}

// ValidateUpdate implements webhook.Validator so a webhook will be registered for the type
func (r *Clusterfed) ValidateUpdate(old runtime.Object) error {
	clusterfedlog.Info("validate update", "name", r.Name)

	return r.validateClusterfed()
}

// ValidateDelete implements webhook.Validator so a webhook will be registered for the type
func (r *Clusterfed) ValidateDelete() error {
	clusterfedlog.Info("validate delete", "name", r.Name)
	return nil
}

func (r *Clusterfed) validateClusterfed() error {
	var allErrs field.ErrorList
	meepoConfig, err := ctrl.GetConfig()
	if err != nil {
		return apierrors.NewServiceUnavailable(string(ServiceGetConfigError))
	}
	wwg := wg.NewWarpGate(wg.Config{
		AuthorizationToken: meepoConfig.BearerToken,
		APIEndpoint:        "http://archon"})
	// Query if the feature gate `a` on cluster `foo` is enabled
	enabled, _ := wwg.IsFeatureGateEnabled("kubefed")
	if !enabled {
		return &apierrors.StatusError{ErrStatus: metav1.Status{
			Status:  metav1.StatusFailure,
			Message: "Federation Cluster is Not Enabeld",
			Reason:  metav1.StatusReasonGone,
			Code:    http.StatusNotImplemented,
		}}
	}
	if err := r.validateClusterFormat(field.NewPath("spec").Child("clusters")); err != nil {
		allErrs = append(allErrs, err)
	}
	if len(allErrs) > 0 {
		return apierrors.NewInvalid(
			schema.GroupKind{Group: "infrastructure.alauda.io", Kind: "Clusterfed"},
			r.Name, allErrs)
	}
	validate, errMsg := r.validateClusterMember()
	if !validate {
		return apierrors.NewBadRequest(errMsg)
	}

	return nil
}

func (r *Clusterfed) validateClusterFormat(fldPath *field.Path) *field.Error {
	hostCount := 0
	for _, cluster := range r.Spec.Clusters {
		if cluster.Type == HostCluster {
			hostCount += 1
		}
	}
	if hostCount != 1 {
		return field.Invalid(fldPath, "clusters", string(HostCountInvalid))
	}
	return nil
}

func (r *Clusterfed) validateClusterMember() (validated bool, errMsg string) {
	managerClient := controlMagager.GetClient()
	var clusterFeds ClusterfedList
	opts := &client.ListOptions{}
	if err := managerClient.List(context.TODO(), &clusterFeds, opts); err != nil {
		return false, "Can't Get clusterfed list"
	} else {
		clusterListNow := make(map[string]string)
		for _, clusterFed := range clusterFeds.Items {
			for _, cluster := range clusterFed.Spec.Clusters {
				clusterListNow[cluster.Name] = clusterFed.Name
			}
		}
		for _, cluster := range r.Spec.Clusters {
			if _, ok := clusterListNow[cluster.Name]; ok && r.Name != clusterListNow[cluster.Name] {
				return false, fmt.Sprintf("Cluster %s Has Been Added to Federation %s", cluster.Name, clusterListNow[cluster.Name])
			}
		}
		return true, ""
	}

}
