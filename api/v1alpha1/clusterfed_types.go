/*

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// ClusterfedSpec defines the desired state of Clusterfed
type ClusterfedSpec struct {
	// Clusters contains the clusters associated with this federation.
	// +kubebuilder:validation:MinItems=1
	Clusters     []*FederatedCluster `json:"clusters,omitempty"`
	UnjoinPolicy UnjoinPolicy        `json:"unjoinpolicy,omitempty"`
}

// UnjoinPolicy describes the behavior when the member cluster unjoin the federation.
// Only one of the following types may be specified.
// If none of the following policies is specified, the default one is Delete.
// +kubebuilder:validation:Enum=Retain;Delete
type UnjoinPolicy string

// FederatedCluster containts the cluster's information in a federation.
type FederatedCluster struct {

	// +kubebuilder:validation:MinLength=0
	// Name of the cluster
	Name string `json:"name"`
	// Specifies how to treat cluster role.
	// Valid values are:
	// - "Host" (default): treat cluster as master;
	// - "Member": treat cluster as a member;
	// +optional
	Type MemberType `json:"type"`
}

// MemberType describes the member cluster type.
// Only one of the following types may be specified.
// If none of the following policies is specified, the default one
// is member.
// +kubebuilder:validation:Enum=Member;Host
type MemberType string

// FederationStatus describes the federation's status.
type FederationStatus string

// InvalidReason describes the reason of rejecting an invalid request.
type InvalidReason string

const (
	// HostCluster represents the cluster is a master cluster in the federation.
	HostCluster MemberType = "Host"

	// MemberCluster represents the cluster is a member cluster in the federation.
	MemberCluster MemberType = "Member"

	// Retain represents the cluster will retain in the project's cluster list when unjoining the federation.
	Retain UnjoinPolicy = "Retain"

	// Delete repreints the cluster will be removed from the project's cluster list when unjoining the federation.
	Delete UnjoinPolicy = "Delete"

	// FederationStatusUnknown status Unknown
	FederationStatusUnknown FederationStatus = "Unknown"
	// FederationStatusCreating status Creating
	FederationStatusCreating FederationStatus = "Creating"
	// FederationStatusActive status Active
	FederationStatusActive FederationStatus = "Active"
	// FederationStatusTerminating status Terminating
	FederationStatusTerminating FederationStatus = "Terminating"
	// FederationStatusError status Error
	FederationStatusError FederationStatus = "Error"

	// HostCountInvalid represents the there are more than one master cluster in the federated clusters.
	HostCountInvalid InvalidReason = "Host Cluster Must Only be One"

	// ServiceGetConfigError represents Meepo can not get token.
	ServiceGetConfigError InvalidReason = "Meepo Can't Get Token"
)

// ClusterfedStatus defines the observed state of Clusterfed
type ClusterfedStatus struct {
	// Phase represents the status of the cluster federation.
	Phase FederationStatus `json:"phase,omitempty"`
	// Reason represents why the cluster federation in this status.
	Reason string `json:"reason,omitempty"`
}

// Clusterfed is the schema for the clusterfeds API
// +kubebuilder:object:root=true
// +kubebuilder:resource:path=clusterfeds,scope=Cluster
// +kubebuilder:subresource:status
type Clusterfed struct {
	metav1.TypeMeta `json:",inline"`
	// +optional
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   ClusterfedSpec   `json:"spec,omitempty"`
	Status ClusterfedStatus `json:"status,omitempty"`
}

//func (in *Clusterfed) Default() {
//	panic("implement me")
//}

// +kubebuilder:object:root=true

// ClusterfedList contains a list of Clusterfed
type ClusterfedList struct {
	metav1.TypeMeta `json:",inline"`
	// +optional
	metav1.ListMeta `json:"metadata,omitempty"`

	// List of Clusterfeds.
	Items []Clusterfed `json:"items"`
}

func init() {
	SchemeBuilder.Register(&Clusterfed{}, &ClusterfedList{})
}

//// SetDefaults make sure that Labels and Annotations are initialized
//func (object *Clusterfed) SetDefaults() {
//	if object.Spec.UnjoinPolicy == "" {
//		object.Spec.UnjoinPolicy = Delete
//	}
//
//	if len(object.Labels) == 0 {
//		object.Labels = map[string]string{}
//	}
//
//	if len(object.Annotations) == 0 {
//		object.Annotations = map[string]string{}
//	}
//
//	if len(object.Finalizers) == 0 {
//		object.Finalizers = []string{"alauda"}
//	}
//}
