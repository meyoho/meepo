package client

import (
	authv1 "bitbucket.org/mathildetech/auth-controller2/pkg/client/clientset/versioned/typed/auth/v1"
	"k8s.io/client-go/rest"
)

// GetAuthV1Client will construct a AuthV1Client to access projects
func GetProjectClient(c *rest.Config) (authv1.ProjectInterface, error) {
	authclient, err := authv1.NewForConfig(c)
	if err != nil {
		return nil, err
	}
	return authclient.Projects(), nil
}
