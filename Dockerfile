FROM alpine:3.11 AS builder
 
COPY ./bin  /opt/output

WORKDIR /opt/output

# pick the architecture
RUN ARCH= && dpkgArch="$(arch)" \
  && case "${dpkgArch}" in \
    x86_64) ARCH='amd64';; \
    aarch64) ARCH='arm64';; \
    *) echo "unsupported architecture"; exit 1 ;; \
  esac \
  && cp linux/${ARCH}/manager manager


FROM alpine:3.11

RUN apk add --no-cache ca-certificates

WORKDIR /

COPY --from=builder /opt/output/manager .

ENTRYPOINT ["./manager"]
