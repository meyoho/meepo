/*

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"fmt"
	"github.com/alauda/helm-crds/pkg/apis/app/v1alpha1"
	"gopkg.in/fatih/set.v0"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/util/workqueue"
	meepoapi "meepo/client"
	"meepo/util"
	"sigs.k8s.io/controller-runtime/pkg/event"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"
	"time"

	authv1 "bitbucket.org/mathildetech/auth-controller2/pkg/apis/auth/v1"
	"github.com/go-logr/logr"
	apiextv1b1 "k8s.io/apiextensions-apiserver/pkg/apis/apiextensions/v1beta1"
	apierror "k8s.io/apimachinery/pkg/api/errors"
	apierrs "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	clusterregistryv1alpha1 "k8s.io/cluster-registry/pkg/apis/clusterregistry/v1alpha1"
	infrastructurev1 "meepo/api/v1alpha1"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	kubefedv1beta1 "sigs.k8s.io/kubefed/pkg/apis/core/v1beta1"
	kubefed "sigs.k8s.io/kubefed/pkg/client/generic"
	"sigs.k8s.io/kubefed/pkg/kubefedctl"
	"sigs.k8s.io/kubefed/pkg/kubefedctl/options"
)

// ClusterfedReconciler reconciles a Clusterfed object
type ClusterfedReconciler struct {
	client.Client
	Log    logr.Logger
	Scheme *runtime.Scheme
}

type ClusterConfig struct {
	ClusterConfig *rest.Config
	ClusterName   string
}

type ClusterLabelManager struct {
	ClusterName          string
	FederationName       string
	HostName             string
	ClusterfedReconciler *ClusterfedReconciler
}

// +kubebuilder:rbac:groups=infrastructure.alauda.io,resources=clusterfeds,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=infrastructure.alauda.io,resources=clusterfeds/status,verbs=get;update;patch
// +kubebuilder:rbac:groups="",resources=secrets,verbs=get;list;watch
// +kubebuilder:rbac:groups="",resources=configmaps,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=clusterregistry.k8s.io,resources=clusters,verbs=get;list;watch;update;patch
// +kubebuilder:rbac:groups=auth.alauda.io,resources=projects,verbs=get;list;watch;update;patchs
// +kubebuilder:rbac:groups="",resources=namespaces,verbs=get;list;watch;create;update;patchs
// +kubebuilder:rbac:groups=app.alauda.io,resources=helmrequests,verbs=get;list;watch;create;update;patchs
// +kubebuilder:rbac:groups=core.kubefed.io,resources=federatedtypeconfigs,verbs=get;list;watch;create;update;patchs;delete

func (r *ClusterfedReconciler) Reconcile(req ctrl.Request) (ctrl.Result, error) {
	ctx := context.Background()
	log := r.Log.WithValues("clusterfed", req.NamespacedName)

	clusterfed := &infrastructurev1.Clusterfed{}
	err := r.Get(ctx, req.NamespacedName, clusterfed)
	if apierrs.IsNotFound(err) {
		// Handle deleted event
		log.Info("clusterfed instance deleted, will delete related member clusters")
		// update to terminating status
		return ctrl.Result{}, nil
	}
	r.Log.Info("clusterfed info", "clusterfed", clusterfed)
	//ensure the creating status
	if clusterfed.Status.Phase == "" {
		clusterfed.Status.Phase = infrastructurev1.FederationStatusCreating
		if len(clusterfed.Finalizers) == 0 {
			clusterfed.Finalizers = []string{util.ClusterFedFinalizers}
			r.Update(ctx, clusterfed)
		}
		if err := r.Status().Update(ctx, clusterfed); err != nil {
			log.Error(err, "unable to update Clusterfed status")
			return ctrl.Result{}, err
		}
	}
	// 1. get host cluster config
	hostConfig, hostErr := r.getHostClusterConfig(ctx, clusterfed)
	if hostErr != nil && clusterfed.DeletionTimestamp == nil {
		log.Error(hostErr, "can't get host cluster info from the crd ", "clusterfed", clusterfed)
		clusterfed.Status.Phase = infrastructurev1.FederationStatusError
		clusterfed.Status.Reason = "Host Cluster is Now Unavailable, Please Check the Status of Host Cluster"
		r.Status().Update(ctx, clusterfed)
		return ctrl.Result{}, hostErr
	}
	if clusterfed.DeletionTimestamp != nil {
		log.Info("clusterfed has DeletionTimestamp", "name", req.Name)
		if clusterfed.Status.Phase == infrastructurev1.FederationStatusTerminating {
			return ctrl.Result{}, nil
		}
		// update to terminating status
		clusterfed.Status.Phase = infrastructurev1.FederationStatusTerminating
		r.Status().Update(ctx, clusterfed)
		r.UnjoinMemberClusters(ctx, clusterfed, hostConfig)
		r.modifyRelatedProjects(clusterfed, hostConfig, true)
		clusterfed.Finalizers = []string{}
		clusterfed.Finalizers = util.RemoveSliceItem(clusterfed.Finalizers, util.ClusterFedFinalizers)
		r.Update(ctx, clusterfed)
		return ctrl.Result{}, err
	}

	// 2. prepare host clusters
	err = r.prepareHostCluster(ctx, clusterfed, hostConfig)
	if apierrs.IsServiceUnavailable(err) {
		scheduledResult := ctrl.Result{RequeueAfter: 5 * time.Second} // save this so we can re-use it elsewhere
		clusterfed.Status.Phase = infrastructurev1.FederationStatusCreating
		clusterfed.Status.Reason = "Host Cluster is not Ready."
		r.Status().Update(ctx, clusterfed)
		return scheduledResult, nil
	}
	// 3.  do sync jobs. join or unjoin
	if err := r.syncFederatedClusters(ctx, clusterfed, hostConfig); err != nil {
		log.Error(err, "sync federated clusters failed ")
	}
	if err := r.Status().Update(ctx, clusterfed); err != nil {
		log.Error(err, "unable to update Clusterfed status")
		return ctrl.Result{}, err
	}

	return ctrl.Result{}, nil
}

func (r *ClusterfedReconciler) syncFederatedClusters(ctx context.Context,
	clusterfed *infrastructurev1.Clusterfed, hostConfig *ClusterConfig) error {
	clusters, err := r.fetchFederatedClusters(ctx, hostConfig)
	if err != nil {
		return err
	}
	federatedClusters := util.GetClusterSetFromKubeFed(clusters)
	alaudaCusters := util.GetClusterSetFromAlaudaFederation(clusterfed.Spec.Clusters)
	// new add should happend
	newJoinCluster := set.StringSlice(set.Difference(alaudaCusters, federatedClusters))
	// unjoin should happend
	unjoinCluster := set.StringSlice(set.Difference(federatedClusters, alaudaCusters))
	scope, err := options.GetScopeFromKubeFedConfig(hostConfig.ClusterConfig, "kube-federation-system")
	if err != nil {
		return err
	}
	// join new clusters
	syncMessage := ""
	for _, alaudaCluster := range newJoinCluster {
		if err := r.joinMemberCluster(ctx, clusterfed, alaudaCluster, hostConfig, scope); err != nil {
			syncMessage = fmt.Sprintf("%s; %s: %s", syncMessage, "Can't Join Cluster", alaudaCluster)
		}
	}
	//unjoin member cluster
	for _, alaudaCluster := range unjoinCluster {
		//may failed, but ignore. because we ignore error when cluster exist in federation
		r.unjoinMemberCluster(ctx, alaudaCluster, hostConfig, true)
	}
	if syncMessage == "" {
		clusterfed.Status.Phase = infrastructurev1.FederationStatusActive
		clusterfed.Status.Reason = ""
	} else {
		clusterfed.Status.Phase = infrastructurev1.FederationStatusError
		clusterfed.Status.Reason = syncMessage
	}

	error := r.Status().Update(ctx, clusterfed)
	if error != nil {
		r.Log.Error(error, "unable to update Clusterfed status")
	}
	// modify project crds
	err = r.modifyRelatedProjects(clusterfed, hostConfig, false)
	if err != nil {
		return err
	}
	return error
}

func (r *ClusterfedReconciler) modifyRelatedProjects(clusterfed *infrastructurev1.Clusterfed, hostConfig *ClusterConfig,
	removeFederation bool) error {
	shouldRetain := true
	var clusterMayDissmissMap map[string]*infrastructurev1.FederatedCluster
	if !removeFederation {
		shouldRetain = clusterfed.Spec.UnjoinPolicy == infrastructurev1.Retain
	}
	projectClient, err := meepoapi.GetProjectClient(ctrl.GetConfigOrDie())
	if err != nil {
		r.Log.Error(err, "unable to get projects client")
		return err
	}
	listOpts := metav1.ListOptions{}
	projectList, err := projectClient.List(listOpts)
	if err != nil {
		r.Log.Error(err, "unable to get project list")
		return err
	}
	clusterFedLabels := make(map[string]string)
	clusterList, _ := r.getClusterRegistryList()
	for _, clusterregistry := range clusterList.Items {
		if len(clusterregistry.Labels) == 0 {
			continue
		}
		if federation, ok := clusterregistry.Labels[util.FederationName]; ok {
			clusterFedLabels[clusterregistry.Name] = federation
		}
	}
	// get clusters which may be dismissed from a federation
	clusterMayDissmissMap = make(map[string]*infrastructurev1.FederatedCluster)
	for _, clusterMayDissmissed := range clusterfed.Spec.Clusters {
		clusterMayDissmissMap[clusterMayDissmissed.Name] = clusterMayDissmissed
	}

	for _, project := range projectList.Items {
		tempClusters := make([]*authv1.ProjectClusters, 0)
		bindded := false
		downGradeToNormal := false
		clusterToConfig := make(map[string]*authv1.ProjectClusters)
		for _, cluster := range project.Spec.Clusters {
			clusterToConfig[cluster.Name] = cluster
			// if bind with current projects
			if federCached, exist := clusterFedLabels[cluster.Name]; exist && federCached == clusterfed.Name {
				// only fedmember matters
				if cluster.Type == util.TypeFedMember {
					bindded = true
				}
				continue
			} else if _, existed := clusterMayDissmissMap[cluster.Name]; !removeFederation && existed { // error join cluster
				continue
			} else if !exist && cluster.Type == util.TypeFedMember {
				if removeFederation {
					if _, existed := clusterMayDissmissMap[cluster.Name]; existed {
						downGradeToNormal = true
					}
				}
				if shouldRetain {
					newCluster := cluster.DeepCopy()
					newCluster.Type = ""
					tempClusters = append(tempClusters, newCluster)
				} else {
					// delete cluster from project if should not retain
					continue
				}
			} else {
				newCluster := cluster.DeepCopy()
				tempClusters = append(tempClusters, newCluster)
			}
		}
		// this project has relation with federation
		if bindded || downGradeToNormal {
			// only bindded project can add new clusters quota.
			if bindded {
				for _, clusterFed := range clusterfed.Spec.Clusters {
					if quotas, exsited := clusterToConfig[clusterFed.Name]; exsited {
						clusterQuota := quotas.DeepCopy()
						clusterQuota.Type = util.TypeFedMember
						tempClusters = append(tempClusters, clusterQuota)
					} else {
						hostQuota, exist := clusterToConfig[hostConfig.ClusterName]
						if exist {
							hostQuotaCopy := hostQuota.DeepCopy()
							hostQuotaCopy.Name = clusterFed.Name
							tempClusters = append(tempClusters, hostQuotaCopy)
						}
					}
				}
			}
			project.Spec.Clusters = tempClusters
			if _, err := projectClient.Update(&project); err != nil {
				r.Log.Error(err, "unable to update project", "project", project)
			}
		}
	}
	return nil
}

// fetchFederatedClusters get federated clusters own by the host.
func (r *ClusterfedReconciler) fetchFederatedClusters(ctx context.Context, hostConfig *ClusterConfig) (clusters *kubefedv1beta1.KubeFedClusterList, err error) {
	hostClient, err := kubefed.New(hostConfig.ClusterConfig)
	var kubeFedClusterList kubefedv1beta1.KubeFedClusterList
	if err := hostClient.List(ctx, &kubeFedClusterList, "kube-federation-system"); err != nil {
		r.Log.Error(err, "unable to list federated clusters")
		return &kubefedv1beta1.KubeFedClusterList{}, err
	}
	return &kubeFedClusterList, nil
}

func (r *ClusterfedReconciler) getHostClusterConfig(ctx context.Context, clusterfed *infrastructurev1.Clusterfed) (*ClusterConfig, error) {
	var hostCluster *infrastructurev1.FederatedCluster
	for _, federatedCluster := range clusterfed.Spec.Clusters {
		// process host member case. install clusterfed control plane
		if federatedCluster.Type == infrastructurev1.HostCluster {
			hostCluster = federatedCluster
			break
		}
	}
	config, err := r.getClusterConfig(ctx, hostCluster.Name)
	if err != nil {
		return &ClusterConfig{}, err
	}
	if _, err := kubefed.New(config); err != nil {
		r.Log.Error(err, "Failed to get host client ", "clustername", hostCluster.Name)
		return &ClusterConfig{
			ClusterConfig: config,
			ClusterName:   hostCluster.Name,
		}, apierrs.NewServiceUnavailable("control plane is installing")
	}
	return &ClusterConfig{
		ClusterConfig: config,
		ClusterName:   hostCluster.Name,
	}, nil
}

func (r *ClusterfedReconciler) getClusterConfig(ctx context.Context, clusterName string) (*rest.Config, error) {
	cluster, e := r.getClusterByName(clusterName)
	if e != nil {
		return nil, e
	}
	return util.BuildClientConfigFromClusterRegistry(ctx, r.Log, r.Client, cluster)
}

func (r *ClusterfedReconciler) getClusterByName(clusterName string) (*clusterregistryv1alpha1.Cluster, error) {
	cluster := &clusterregistryv1alpha1.Cluster{}
	namespace := util.GetEnv("LEADER_ELECTION_NAMESPACE", "alauda-system")
	err := r.Get(context.TODO(), types.NamespacedName{Namespace: namespace, Name: clusterName}, cluster)
	if err != nil {
		r.Log.Error(err, "can't get cluster info", "clusterName", cluster.Name)
		return nil, err
	}
	return cluster, nil
}

func (r *ClusterfedReconciler) getClusterRegistryList() (*clusterregistryv1alpha1.ClusterList, error) {
	clusterList := &clusterregistryv1alpha1.ClusterList{}
	namespace := util.GetEnv("LEADER_ELECTION_NAMESPACE", "alauda-system")
	err := r.List(context.TODO(), clusterList, client.InNamespace(namespace))
	if err != nil {
		r.Log.Error(err, "can't get cluster list", "namespace", namespace)
		return nil, err
	}
	return clusterList, nil
}

// isKubeFedControlPlaneOK check if kubefed control plane installed.
func (r *ClusterfedReconciler) isKubeFedControlPlaneOK(ctx context.Context, hostConfig *ClusterConfig) (bool, error) {
	namespace := util.GetEnv("LEADER_ELECTION_NAMESPACE", "alauda-system")
	var helmRequest v1alpha1.HelmRequest
	hostName := fmt.Sprintf("%s-%s", "kubefed", hostConfig.ClusterName)
	err := r.Get(context.TODO(), types.NamespacedName{Namespace: namespace, Name: hostName}, &helmRequest)
	if apierrs.IsNotFound(err) {
		return false, err
	}

	client, err := kubefed.New(hostConfig.ClusterConfig)
	if err != nil {
		r.Log.Error(err, "Failed to get kubefed", "clientname", hostConfig.ClusterName)
		return true, apierrs.NewServiceUnavailable("control plane is installing")
	}

	fedConfig := &kubefedv1beta1.KubeFedConfig{}
	if err = client.Get(ctx, fedConfig, "kube-federation-system", "kubefed"); err != nil {
		r.Log.Info("A KubeFedConfig named %q was not found in namespace %q. Is a KubeFed control plane running in this namespace?",
			"kubefed", "kube-federation-system")
		return true, apierrs.NewServiceUnavailable("control plane is installing")
	} else {
		fedWebhookEndpoints := &corev1.Endpoints{}
		svcError := client.Get(ctx, fedWebhookEndpoints, "kube-federation-system", "kubefed-admission-webhook")
		epAvailable := false
		for _, subnet := range fedWebhookEndpoints.Subsets {
			if len(subnet.Addresses) > 0 {
				epAvailable = true
				break
			}
		}
		if svcError != nil || !epAvailable {
			return true, apierrs.NewServiceUnavailable("control plane is installing")
		}
		return true, nil
	}
}

// isKubeFedControlPlaneOK check if kubefed control plane installed.
func (r *ClusterfedReconciler) installKubeFedControlPlaneOK(ctx context.Context, kubefed *infrastructurev1.Clusterfed,
	hostConfig *ClusterConfig) (bool, error) {
	if kubefed.Status.Phase != infrastructurev1.FederationStatusCreating {
		kubefed.Status.Phase = infrastructurev1.FederationStatusCreating
		kubefed.Status.Reason = "installing control plane."
		if err := r.Status().Update(ctx, kubefed); err != nil {
			r.Log.Error(err, "unable to update Clusterfed status")
			return false, err
		}
	}
	r.createHostNamespace(hostConfig)
	constructHelmRequest := func(kubefed *infrastructurev1.Clusterfed, hostConfig *ClusterConfig) (*v1alpha1.HelmRequest, error) {
		name := fmt.Sprintf("%s-%s", "kubefed", hostConfig.ClusterName)
		namespace := util.GetEnv("LEADER_ELECTION_NAMESPACE", "alauda-system")
		chartRepo := util.GetEnv("CHART_REPO_ADDRESS", "stable")
		chartVersion := util.GetEnv("CHART_REPO_VERSION", "0.0.2")
		helmValue := make(map[string]interface{})
		registryValue := make(map[string]interface{})
		addressValue := make(map[string]interface{})
		addressValue["address"] = util.GetEnv("KUBEFED_REPO_ADDRESS", "192.168.16.52:60080")
		registryValue["registry"] = addressValue
		helmValue["global"] = registryValue
		hr := &v1alpha1.HelmRequest{
			ObjectMeta: metav1.ObjectMeta{
				Labels:      make(map[string]string),
				Annotations: make(map[string]string),
				Name:        name,
				Namespace:   namespace,
			},
			Spec: v1alpha1.HelmRequestSpec{
				ClusterName:          hostConfig.ClusterName,
				InstallToAllClusters: false,
				Dependencies:         nil,
				ReleaseName:          "kubefed",
				Chart:                fmt.Sprintf("%s/kubefed", chartRepo),
				Version:              chartVersion,
				Namespace:            "kube-federation-system",
				ValuesFrom:           nil,
				HelmValues: v1alpha1.HelmValues{
					Values: helmValue,
				},
			},
		}
		if err := ctrl.SetControllerReference(kubefed, hr, r.Scheme); err != nil {
			return nil, err
		}

		return hr, nil
	}
	// +kubebuilder:docs-gen:collapse=constructHelmRequest

	// actually make the HelmRequest...
	hr, err := constructHelmRequest(kubefed, hostConfig)
	if err != nil {
		r.Log.Error(err, "unable to construct Helm Request from template")
		return false, nil
	}
	// ...and create it on the cluster
	if err := r.Create(ctx, hr); err != nil {
		r.Log.Error(err, "unable to create Helm Request for kubefed", "kubefed", kubefed)
		return false, err
	}
	return true, nil
}

func (r *ClusterfedReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&infrastructurev1.Clusterfed{}).
		Watches(&source.Kind{Type: &clusterregistryv1alpha1.Cluster{}}, &handler.Funcs{
			UpdateFunc: func(evt event.UpdateEvent, q workqueue.RateLimitingInterface) {
				if evt.MetaNew == nil {
					r.Log.Error(nil, "UpdateEvent received with no metadata", "event", evt)
					return
				}
				labels := evt.MetaNew.GetLabels()
				federationName, existed := labels[util.FederationName]
				if existed {
					q.Add(reconcile.Request{
						NamespacedName: types.NamespacedName{
							Name: federationName,
						}})
				}
			},
		}).
		Complete(r)
}

func (r *ClusterfedReconciler) prepareHostCluster(ctx context.Context, kubefed *infrastructurev1.Clusterfed, hostConfig *ClusterConfig) error {
	//if service not availlable we should not install again
	if controPlaneInstalled, error := r.isKubeFedControlPlaneOK(ctx, hostConfig); !controPlaneInstalled {
		if _, err := r.installKubeFedControlPlaneOK(ctx, kubefed, hostConfig); err != nil {
			r.Log.Error(err, "install kubefed control plane error",
				"clusterName", hostConfig.ClusterName)
		}
		return apierrs.NewServiceUnavailable("control plane is installing")
	} else if apierrs.IsServiceUnavailable(error) {
		return error
	} else {
		return nil
	}
}

func (r *ClusterfedReconciler) UnjoinMemberClusters(ctx context.Context, clusterfed *infrastructurev1.Clusterfed, hostConfig *ClusterConfig) {
	//unjoin member cluster
	for _, federatedCluster := range clusterfed.Spec.Clusters {
		r.unjoinMemberCluster(ctx, federatedCluster.Name, hostConfig, true)
	}
}

func (r *ClusterfedReconciler) unjoinMemberCluster(ctx context.Context, federatedCluster string, hostConfig *ClusterConfig, ignoreError bool) error {
	businessClusterConfig, err := r.getClusterConfig(ctx, federatedCluster)
	if err != nil {
		r.Log.Error(err, "get unjoin member cluster info error", "member cluster", federatedCluster,
			"host cluster", hostConfig.ClusterName)
		return err
	}
	if err := kubefedctl.UnjoinCluster(hostConfig.ClusterConfig, businessClusterConfig, "kube-federation-system",
		hostConfig.ClusterName, "", federatedCluster, true, false); err != nil && !ignoreError {
		r.Log.Error(err, "unjoin cluster error", "member cluster", federatedCluster,
			"host cluster", hostConfig.ClusterName)
		return err
	}
	clm := &ClusterLabelManager{
		ClusterName:          federatedCluster,
		ClusterfedReconciler: r,
	}
	util.LoopUntil(ctx, 1*time.Second, 5, clm.unsetCluterLabel)

	return nil
}

func (r *ClusterfedReconciler) joinMemberCluster(ctx context.Context, clusterfed *infrastructurev1.Clusterfed, alaudaCluster string,
	hostConfig *ClusterConfig, scope apiextv1b1.ResourceScope) error {
	businessClusterConfig, err := r.getClusterConfig(ctx, alaudaCluster)
	if err != nil {
		r.Log.Error(err, "get member cluster info error", "member cluster", alaudaCluster,
			"host cluster", hostConfig.ClusterName)
		return err
	}
	if _, err := kubefedctl.JoinCluster(hostConfig.ClusterConfig, businessClusterConfig, "kube-federation-system",
		hostConfig.ClusterName, alaudaCluster, "", scope, false, false); err != nil {
		r.Log.Error(err, "join cluster error", "member cluster", alaudaCluster,
			"host cluster", hostConfig.ClusterName)
		return err
	}
	clm := &ClusterLabelManager{
		ClusterName:          alaudaCluster,
		FederationName:       clusterfed.Name,
		HostName:             hostConfig.ClusterName,
		ClusterfedReconciler: r,
	}
	util.LoopUntil(ctx, 1*time.Second, 5, clm.setClusterLabel)
	return nil
}

func (c *ClusterLabelManager) setClusterLabel() (bool, error) {
	clusterregistry, err := c.ClusterfedReconciler.getClusterByName(c.ClusterName)
	if len(clusterregistry.Labels) == 0 {
		clusterregistry.Labels = map[string]string{}
	}
	clusterregistry.Labels[util.FederationName] = c.FederationName
	clusterregistry.Labels[util.FederationHostName] = c.HostName
	err = c.ClusterfedReconciler.Update(context.TODO(), clusterregistry)
	if err != nil {
		c.ClusterfedReconciler.Log.Error(err, "modify clusterregistry error ", "member cluster", c.ClusterName)
		return false, err
	}
	return true, nil
}

func (c *ClusterLabelManager) unsetCluterLabel() (bool, error) {
	clusterregistry, err := c.ClusterfedReconciler.getClusterByName(c.ClusterName)
	if len(clusterregistry.Labels) == 0 {
		return true, nil
	}
	if _, hasFedLabel := clusterregistry.Labels[util.FederationName]; hasFedLabel {
		delete(clusterregistry.Labels, util.FederationName)
	}
	if _, hasHostLabel := clusterregistry.Labels[util.FederationHostName]; hasHostLabel {
		delete(clusterregistry.Labels, util.FederationHostName)
	}
	err = c.ClusterfedReconciler.Update(context.TODO(), clusterregistry)
	if err != nil {
		c.ClusterfedReconciler.Log.Error(err, "modify clusterregistry error ", "member cluster", c.ClusterName,
			"host cluster", c.HostName)
		return false, err
	}
	return true, nil
}

// createNamespace will create ns with same name with Project on global and the ownerReference is set.
func (r *ClusterfedReconciler) createHostNamespace(hostConfig *ClusterConfig) error {
	namespace := util.NewHostNamespace()
	clientset, err := kubernetes.NewForConfig(hostConfig.ClusterConfig)
	if err != nil {
		r.Log.Error(err, "Namespace create error", "hostConfig", hostConfig)
		return err
	}
	_, createNamespaceErr := clientset.CoreV1().Namespaces().Create(namespace)

	//create success
	if createNamespaceErr == nil {
		r.Log.Info("Namespace created successfully", "name", namespace.Name)
	} else if !apierror.IsAlreadyExists(createNamespaceErr) {
		r.Log.Error(createNamespaceErr, "Namespace create error", "name", util.FederationHostName)
		return createNamespaceErr
	}
	return nil
}
